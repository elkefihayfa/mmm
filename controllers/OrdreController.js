const ordreModel = require('../models/Ordre');

module.exports = {

    create: function(req, res, next) {

        ordreModel.create({  ref: req.body.ref , prix : req.body.prix , des : req.body.des , name : req.body.name, user : req.body.user }, function (err, result) {
            if (err)
                next(err);
            else
                res.json({status: "success", message: "User added successfully!!!", data: null});

        });
    },



    delete : function (req, res, next ) {

        ordreModel.remove({_id:req.body.id} , function (err , result ) {
            if (err)
                next(err) ;

            else
                res.json({status: "success", message: "User deleted successfully!!!", data: null});

        });
    } ,


    //////////////update ////////////////




    update : function (req, res, next ) {

        ordreModel.update({ _id : req.body.id} ,{  ref: req.body.ref , prix : req.body.prix , des : req.body.des , name : req.body.name, user : req.body.user }, function (err, result) {

            if (err)
                next(err) ;

            else
                res.json({status: "success", message: "User updated successfully!!!", data: null});

        });
    } ,



    findAll : function (req, res, next ) {


        //predefini orm

        ordreModel.find({}, function (err, result) {
            if (err)

                next(err) ;


            else
                res.json({status: "success", message: "",  data : result});

        });
    } ,







}