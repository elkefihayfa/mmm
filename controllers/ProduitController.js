const produitModel = require('../models/Produit');

module.exports = {

    create: function(req, res, next) {

        produitModel.create({ ref: req.body.ref, design: req.body.design , prix : req.body.prix,subCat:req.body.subCat}, function (err, result) {
            if (err)
                next(err);
            else
                res.json({status: "success", message: "User added successfully!!!", data: null});

        });
    },

    delete : function (req, res, next ) {

        produitModel.remove({_id:req.body.id} , function (err , result ) {
            if (err)
                next(err) ;

            else
                res.json({status: "success", message: "User deleted successfully!!!", data: null});

        });
    } ,


    //////////////update ////////////////




    update : function (req, res, next ) {

        produitModel.update({ _id : req.body.id} , { ref: req.body.ref, design: req.body.design , prix : req.body.prix,subCat:req.body.subCat}, function (err, result) {

            if (err)
                next(err) ;

            else
                res.json({status: "success", message: "User updated successfully!!!", data: null});

        });
    } ,



    findAll : function (req, res, next ) {


        //predefini orm

        produitModel.find({}, function (err, result) {
            if (err)

                next(err) ;


            else
                res.json({status: "success", message: "User updated successfully!!!",  data : result});

        });
    } ,






}