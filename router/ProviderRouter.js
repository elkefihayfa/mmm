const express = require('express');
const router = express.Router();
const providerController = require('../controllers/ProviderController');

router.post('/create', providerController.create);


router.delete('/delete', providerController.delete);


router.put('/update', providerController.update);



router.get('/findAll', providerController.findAll);


module.exports = router;