const express = require('express');
const router = express.Router();
const OrdreController = require('../controllers/OrdreController');
router.post('/create', OrdreController.create);


router.delete('/delete', OrdreController.delete);


router.put('/update', OrdreController.update);



router.get('/findAll', OrdreController.findAll);



module.exports = router;