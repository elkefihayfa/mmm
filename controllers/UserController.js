const userModel = require('../models/User');

module.exports = {

    create: function(req, res, next) {

        userModel.create({ name: req.body.name, email: req.body.email,password: req.body.password}, function (err, result) {

            if (err)
                next(err);
            else
                res.json({status: "success", message: "User added successfully!!!", data: null});

        });
    },


    delete : function (req, res, next ) {

        userModel.remove({_id:req.body.id} , function (err , result ) {
             if (err)
                 next(err) ;

             else
                 res.json({status: "success", message: "User deleted successfully!!!", data: null});

        });
    } ,


    //////////////update ////////////////




    update : function (req, res, next ) {

        userModel.update({ _id : req.body.id} , {name: req.body.name, email: req.body.email,password: req.body.password}, function (err, result) {

            if (err)
                next(err) ;

            else
                res.json({status: "success", message: "User updated successfully!!!", data: null});

        });
    } ,



    findAll : function (req, res, next ) {


        //predefini orm

        userModel.find({}, function (err, result) {
            if (err)

                next(err) ;


            else
                res.json({status: "success", message: "User updated successfully!!!",  data : result});

        });
    } ,







}