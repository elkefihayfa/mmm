const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const baseOptions = {
    discriminatorKey: '__type',
    collection: 'users'
}


//Define a schema
const Schema = mongoose.Schema;
const UserSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: true,
    },
    email: {
        type: String,
        trim: true,
        required: true
    },
    password: {
        type: String,
        trim: true,
        required: true
    } ,

}, baseOptions);





// hash user password before saving into database
UserSchema.pre('save', function(next){
    this.password = bcrypt.hashSync(this.password, saltRounds);
    next();
});
module.exports = mongoose.model('User', UserSchema);