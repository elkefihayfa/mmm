const express = require('express');
const router = express.Router();
const delivryController = require('../controllers/DelivryManController');

router.post('/create', delivryController.create);


router.delete('/delete', delivryController.delete);


router.put('/update', delivryController.update);



router.get('/findAll', delivryController.findAll);


module.exports = router;