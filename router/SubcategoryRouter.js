const express = require('express');
const router = express.Router();
const SubcategoryController = require('../controllers/SubcategoryController');


router.post('/create', SubcategoryController.create);

router.delete('/delete', SubcategoryController.delete);


router.put('/update', SubcategoryController.update);



router.get('/findAll', SubcategoryController.findAll);




module.exports = router;