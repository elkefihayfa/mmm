const express = require('express');
const router = express.Router();
const ProduitController = require('../controllers/ProduitController');


router.post('/create', ProduitController.create);


router.delete('/delete', ProduitController.delete);


router.put('/update', ProduitController.update);



router.get('/findAll', ProduitController.findAll);



module.exports = router;