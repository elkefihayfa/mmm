let mongoose = require('mongoose')


let ordreSchema = new mongoose.Schema({

    ref: Number,

    prix: Number,

    description: String,

    name: String ,

    user:{
        type: mongoose.Schema.ObjectId,
        ref: 'User' // Reference to sub category  relation 1 *
    }


})

module.exports = mongoose.model('Ordre', ordreSchema)
